﻿# Описание

Шаблон для создания [reveal.js](http://lab.hakim.se/reveal-js) презентаций и pdf файлов из adoc документа.

Уже скачан reveal.js backend для asciidoctcor.

Настроены иконки font awesome в шаблоне.

Инструкция в guide.adoc
